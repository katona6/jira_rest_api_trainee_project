package JIRALekerdez;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import com.atlassian.jira.rest.client.NullProgressMonitor;
import com.atlassian.jira.rest.client.domain.BasicProject;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.domain.IssueType;
import com.atlassian.jira.rest.client.domain.IssuelinksType;
import com.atlassian.jira.rest.client.domain.Priority;
import com.atlassian.jira.rest.client.domain.Project;
import com.atlassian.jira.rest.client.domain.ProjectRole;
import com.atlassian.jira.rest.client.domain.Resolution;
import com.atlassian.jira.rest.client.domain.ServerInfo;
import com.atlassian.jira.rest.client.domain.Session;
import com.atlassian.jira.rest.client.domain.Status;
import com.atlassian.jira.rest.client.domain.Transition;
import com.atlassian.jira.rest.client.domain.User;
import com.atlassian.jira.rest.client.domain.Votes;
import com.atlassian.jira.rest.client.domain.Watchers;
import com.atlassian.jira.rest.client.internal.jersey.JerseyJiraRestClientFactory;


public interface IJiraServiceImpl {
	public void getConnection(Properties props) throws URISyntaxException;
	public void getJson(String url);
	public Project getProjectByKey(String key);
	public Issue getIssueByKey(String key);
	public Iterable <BasicProject> getAllProjects();
	public Watchers getWatchersOfAnIssue(Properties props,String IssueKey) throws URISyntaxException;
	public Iterable <Transition> getTransitionsOfAnIssue(Properties prop, String IssueKey) throws URISyntaxException;
	public Votes getVotesOfAnIssue(Properties prop,String IssueKey) throws URISyntaxException;
	public Iterable <IssuelinksType> getIssueLinkTypes();
	public Iterable <Priority> getPriorities();
	public ServerInfo getServerInfo();
	public Iterable <ProjectRole> getProjectRoles(String ProjectURI) throws URISyntaxException;
	public Session getCurrentSession();
	public Status getStatusOfAnIssue(Properties prop, String RequestStatusURI) throws URISyntaxException;
	public Resolution getResolution(Properties prop, String ResolutionURI) throws URISyntaxException;
	public IssueType getIssueType(Properties prop, String IssueTypeURI) throws URISyntaxException;
	public User getUser(Properties prop, String Username) throws URISyntaxException;
	public Priority getIssuePriority(Properties prop, String PriorityID) throws URISyntaxException;
	public void createIssue();
	//public void doComment();

}
