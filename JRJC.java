import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.NullProgressMonitor;
import com.atlassian.jira.rest.client.ProgressMonitor;
import com.atlassian.jira.rest.client.domain.BasicProject;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.domain.IssuelinksType;
import com.atlassian.jira.rest.client.domain.Priority;
import com.atlassian.jira.rest.client.domain.Project;
import com.atlassian.jira.rest.client.domain.ProjectRole;
import com.atlassian.jira.rest.client.domain.ServerInfo;
import com.atlassian.jira.rest.client.domain.Session;
import com.atlassian.jira.rest.client.domain.Status;
import com.atlassian.jira.rest.client.domain.Transition;
import com.atlassian.jira.rest.client.domain.Votes;
import com.atlassian.jira.rest.client.domain.Watchers;
import com.atlassian.jira.rest.client.internal.jersey.JerseyJiraRestClientFactory;

public class JRJC {
	static JiraRestClient restClient;
	static ProgressMonitor pm;

	public JRJC() {
	}

	public static void getConnection(Properties props) throws URISyntaxException {
		final JerseyJiraRestClientFactory factory = new JerseyJiraRestClientFactory();
		final URI jiraServerUri = new URI("http://jrserv.simplesoft.hu:81");
		restClient = factory.createWithBasicHttpAuthentication(jiraServerUri, "gyakornok", "gyakornokpw");
		pm = new NullProgressMonitor();
	}

	public void getJson(String url) {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet post = new HttpGet(url);
		HttpResponse response = null;
		try {
			response = httpClient.execute(post);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		HttpEntity resEntity = response.getEntity();
		String json = "";
		if (resEntity != null) {
			try {
				json = EntityUtils.toString(resEntity);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		httpClient.getConnectionManager().shutdown();
		System.out.println(json.toString());
	}

	/*OBJECT-t visszaad� met�dusok  
	  ----->  */

	public static Project getProjectByKey(String key){
		return restClient.getProjectClient().getProject(key, pm);
	}
	
	public static Issue getIssueByKey(String key){
		return restClient.getIssueClient().getIssue(key, pm);
	}
	
	public static Iterable <BasicProject> getAllProjects(){
		return restClient.getProjectClient().getAllProjects(pm);
	}
	
	public static String getProjectClient(){
		return restClient.getProjectClient().toString();	
	}
	
	public static String getIssueClient(){
		return restClient.getIssueClient().toString();
	}
	
	//*****************************************************************************
	
	public static Watchers getWatchersOfAnIssue(String IssueKey) throws URISyntaxException{
		URI jiraServerUri = new URI("http://jrserv.simplesoft.hu:81/rest/api/2.0.alpha1/issue/"+IssueKey+"/watchers");
		return restClient.getIssueClient().getWatchers(jiraServerUri, pm);
		
	}
	
	public static Iterable <Transition> getTransitionsOfAnIssue(String IssueKey){
		Issue issue = restClient.getIssueClient().getIssue(IssueKey, pm);
		return restClient.getIssueClient().getTransitions(issue, pm);
	//*****************************************************************************
		
	}
	
	public static Votes getVotesOfAnIssue(String IssueKey) throws URISyntaxException{
		URI jiraServerUri = new URI("http://jrserv.simplesoft.hu:81/rest/api/2.0.alpha1/issue/"+IssueKey+"/votes");
		return restClient.getIssueClient().getVotes(jiraServerUri, pm);
		
	}
	
	public static Iterable <IssuelinksType> getIssueLinkTypes(){
		return restClient.getMetadataClient().getIssueLinkTypes(pm);
	}
	
	public static Iterable <Priority> getPriorities(){
		return restClient.getMetadataClient().getPriorities(pm);
	}
	
	public static ServerInfo getServerInfo(){
		return restClient.getMetadataClient().getServerInfo(pm);
	}
	
	public static Iterable <ProjectRole> getProjectRoles(String ProjectURI) throws URISyntaxException{
		URI jiraServerUri = new URI(ProjectURI);
		return restClient.getProjectRolesRestClient().getRoles(jiraServerUri, pm);
		
	}
	
	public static Session getCurrentSession(){
		return restClient.getSessionClient().getCurrentSession(pm);
	}
	
	public static Status getStatusOfAnIssue(String RequestStatusURI) throws URISyntaxException{
		URI ueri = new URI(RequestStatusURI);
		return restClient.getMetadataClient().getStatus(ueri, pm);
	}
	
	public static void main(String[] args) throws URISyntaxException {
		JRJC conn = new JRJC();
		//getConnection();

		//final Project project = restClient.getProjectClient().getProject("GYAKPROJ", pm);
		//System.out.println(project + "\n");
		System.out.println("getProjectByKey : \n"+getProjectByKey("GYAKPROJ") + "\n");	// ez az el�bbi 2 sort helyettesiti

		//final Issue issue = restClient.getIssueClient().getIssue("GYAKPROJ-2", pm);
		//System.out.println(issue + "\n");
		System.out.println("getIssueByKey : \n"+ getIssueByKey("GYAKPROJ-2") + "\n");	// ez az el�bbi 2 sort helyettesiti
		
		
		System.out.println("getAllProjects :");
		Iterable <BasicProject> bp = restClient.getProjectClient().getAllProjects(pm);
		Iterable <BasicProject> bpm = getAllProjects();
		for (BasicProject basicProject : bpm) {
			System.out.println(basicProject);
		}
		
		System.out.println("\ngetProjectClient : \n"+ getProjectClient() );

		
		System.out.println("\n");
		System.out.println("getWatchersOfAnIssue : \n"+ getWatchersOfAnIssue("GYAKPROJ-2")  );
		
		
		System.out.println("\ngetTransitionsOfAnIssue :");
		Iterable <Transition> T2 = getTransitionsOfAnIssue("GYAKPROJ-2");
		for (Transition transition : T2) {
			System.out.println(transition);
		}

		
		Votes votes = getVotesOfAnIssue("GYAKPROJ-2");
		System.out.println("\ngetVotesOfAnIssue : \n"+ votes);
		
		
		System.out.println("\ngetIssueClient : \n"+ getIssueClient() );
		
		
		System.out.println("\ngetIssueLinkTypes :");
		Iterable <IssuelinksType> ILT = getIssueLinkTypes();
		for (IssuelinksType issuelinksType : ILT) {
			System.out.println(issuelinksType);
		}
		
//		System.out.println("\ngetPriorities :");
//		Iterable <Priority> p = getPriorities();
//		for (Priority priority : p) {
//			System.out.println(priority);
//		}
		
		
		System.out.println("\ngetServerInfo : \n" + getServerInfo());
		
		
//		System.out.println("\ngetProjectRoles :");
//		Iterable <ProjectRole> pr = getProjectRoles("http://jrserv.simplesoft.hu:81/rest/api/2.0.alpha1/project/GYAKPROJ");
//		for (ProjectRole projectRole : pr) {
//			System.out.println(projectRole);
//		}
		
		System.out.println("\ngetCurrentSession: \n"+ getCurrentSession());
				
		
//		IssueInput ii = new IssueInput(fields);
//		restClient.getIssueClient().createIssue(ii, pm);
		
		
		System.out.println("\ngetStatusOfAnIssue : \n" + getStatusOfAnIssue("http://jrserv.simplesoft.hu:81/rest/api/2.0.alpha1/status/1"));
		
//		CREATE ISSUE
//        final Iterable<CimProject> createIssueMetadata = restClient.getIssueClient()
//        .getCreateIssueMetadata(new GetCreateIssueMetadataOptionsBuilder().build(), pm);
//		System.out.println(createIssueMetadata);
//	    final IssueInputBuilder issueBuilder = new IssueInputBuilder("GYAKPROJ", 1l);
//        issueBuilder.setDescription("description");
//        issueBuilder.setSummary("summary");
//        BasicIssue jiraIssue = restClient.getIssueClient().createIssue(issueBuilder.build(), pm);
//        System.out.println(jiraIssue);
		
		//BasicIssue jiraIssue = issueClient.createIssue(issueInput, pm);		
		

		
		 //conn.getJson("http://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=false");   //GOOGLE JSON

	}
}
