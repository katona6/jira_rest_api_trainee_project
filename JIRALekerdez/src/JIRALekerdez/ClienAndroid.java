package JIRALekerdez;
import java.net.URISyntaxException;
import java.util.Properties;

import com.atlassian.jira.rest.client.domain.BasicProject;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.domain.IssueType;
import com.atlassian.jira.rest.client.domain.IssuelinksType;
import com.atlassian.jira.rest.client.domain.Priority;
import com.atlassian.jira.rest.client.domain.Project;
import com.atlassian.jira.rest.client.domain.Resolution;
import com.atlassian.jira.rest.client.domain.Status;
import com.atlassian.jira.rest.client.domain.Transition;
import com.atlassian.jira.rest.client.domain.User;
import com.atlassian.jira.rest.client.domain.Votes;
import com.atlassian.jira.rest.client.domain.Watchers;


public class ClienAndroid {

	public static void main(String[] args) throws URISyntaxException {
		Properties properties = new Properties();
		properties.put("serverurl", "http://jrserv.simplesoft.hu:81");
		properties.put("issueurl", "http://jrserv.simplesoft.hu:81/rest/api/2.0.alpha1/issue");
		properties.put("projecturl", "http://jrserv.simplesoft.hu:81/rest/api/2.0.alpha1/project");
		properties.put("user", "gyakornok");
		properties.put("password", "gyakornokpw");
		
		// Code to an interface - code to interface
		JiraServiceImpl service = new JiraServiceImpl(properties);
		
		//GET PROJECT
		Project project = service.getProjectByKey("GYAKPROJ");
		System.out.println("getProjectByKey:\n"+project);
		System.out.println("getProjectByKey - Components:\n"+project.getComponents());
		System.out.println("getProjectByKey - Description:\n"+project.getDescription()    );
		System.out.println("getProjectByKey - Project key:\n"+project.getKey()    );
		System.out.println("getProjectByKey - Project Name:\n"+project.getName()    );
		System.out.println("getProjectByKey - Project Self:\n"+project.getSelf()    );
		System.out.println("getProjectByKey - Project URI:\n"+project.getUri()    );
		System.out.println("getProjectByKey - Project Version:\n"+project.getVersions()    );
		
		//GET ISSUE
		Issue issue = service.getIssueByKey("GYAKPROJ-2");
		System.out.println("\ngetIssueByKey:\n"+issue);
		System.out.println("Description		:"+issue.getDescription());
		System.out.println("Key			:"+issue.getKey());
		System.out.println("Summary			:"+issue.getSummary());
		System.out.println("Attachments		:"+issue.getAttachments());
		System.out.println("Comments		:"+issue.getComments());
		System.out.println("Components		:"+issue.getComponents());
		System.out.println("Creation Date		:"+issue.getCreationDate());
		System.out.println("Priority		:"+issue.getPriority());
		System.out.println("Resolution		:"+issue.getResolution());
		System.out.println("SELF			:"+issue.getSelf());
		System.out.println("Status			:"+issue.getStatus());
		System.out.println("Update Date		:"+issue.getUpdateDate());
		System.out.println("Votes			:"+issue.getVotes());
		
		// GET ALL PROJECTS
		System.out.println("\ngetAllProjects :");
		Iterable <BasicProject> bpm = service.getAllProjects();
		for (BasicProject basicProject : bpm) {
			System.out.println("THE Project is:\n"+basicProject);
			System.out.println("Project key:"+basicProject.getKey());
			System.out.println("Project name:"+basicProject.getName());
			System.out.println("Project SELF:"+basicProject.getSelf());
		}
		
		// ISSUE WATCHERS
		System.out.println("\ngetWatchersOfAnIssue :");
		Watchers watchers =  service.getWatchersOfAnIssue(properties,"GYAKPROJ-2");
		System.out.println(watchers);
		System.out.println("Number of Watchers:" + watchers.getNumWatchers());
		System.out.println("Watchers SELF:" + watchers.getSelf());
		System.out.println("Users:" + watchers.getUsers());
		System.out.println("Is Watching:" + watchers.isWatching());
		
		
		// ISSUE VOTES
		Votes votes = service.getVotesOfAnIssue(properties,"GYAKPROJ-2");
		System.out.println("\ngetVotesOfAnIssue : \n"+ votes);
		System.out.println("getVotesOfAnIssue - getVotes : "+ votes.getVotes());
		System.out.println("getVotesOfAnIssue - SELF : "+ votes.getSelf());
		System.out.println("getVotesOfAnIssue - Users : "+ votes.getUsers());
		System.out.println("getVotesOfAnIssue - hasVoted : "+ votes.hasVoted());
		
		// ISSUE LINK TYPES
		System.out.println("\ngetIssueLinkTypes :");
		Iterable <IssuelinksType> ILT = service.getIssueLinkTypes();
		for (IssuelinksType issuelinksType : ILT) {
			System.out.println(issuelinksType);
			System.out.println("ID:"+issuelinksType.getId());
			System.out.println("Inward:"+issuelinksType.getInward());
			System.out.println("Name:"+issuelinksType.getName());
			System.out.println("Outward:"+issuelinksType.getOutward());
			System.out.println("SELF:"+issuelinksType.getSelf());
		}		
		
		// SERVER INFO
		System.out.println("\ngetServerInfo : \n" + service.getServerInfo());
		System.out.println("Build Number : " + service.getServerInfo().getBuildNumber());
		System.out.println("SCM Info : " + service.getServerInfo().getScmInfo());
		System.out.println("Server Title : " + service.getServerInfo().getServerTitle());
		System.out.println("Version : " + service.getServerInfo().getVersion());
		System.out.println("Base URI : " + service.getServerInfo().getBaseUri());
		System.out.println("Build Date : " + service.getServerInfo().getBuildDate());
		System.out.println("Server Time : " + service.getServerInfo().getServerTime());
		
		// CURRENT SESSION
		System.out.println("\ngetCurrentSession: \n"+ service.getCurrentSession());
		System.out.println("Username: "+ service.getCurrentSession().getUsername());
		System.out.println("Failed Login: "+ service.getCurrentSession().getLoginInfo().getFailedLoginCount());
		System.out.println("All Login: "+ service.getCurrentSession().getLoginInfo().getLoginCount());
		System.out.println("Last Failed Login Date: "+ service.getCurrentSession().getLoginInfo().getLastFailedLoginDate());
		System.out.println("Previous Login Date: "+ service.getCurrentSession().getLoginInfo().getPreviousLoginDate());
		System.out.println("User URI: "+ service.getCurrentSession().getUserUri());
		
		// ISSUE STATUS
		System.out.println("\ngetStatusOfAnIssue :");
		Status status = service.getStatusOfAnIssue(properties , "/rest/api/2.0.alpha1/status/1");
		System.out.println(status);
		System.out.println("Description:" + status.getDescription());
		System.out.println("Name:" + status.getName());
		System.out.println("Icon URL:" + status.getIconUrl());
		System.out.println("SELF:" + status.getSelf());
		
		// GET RESOLUTION
		System.out.println("\ngetResolution:");
		Resolution res = service.getResolution(properties, "/rest/api/2.0.alpha1/resolution/1");
		System.out.println(res);
		System.out.println("Description :" + res.getDescription());
		System.out.println("Name :" + res.getName());
		System.out.println("SELF :" + res.getSelf());
		
		// ISSUE TYPE
		System.out.println("\ngetIssueType:");
		IssueType it =  service.getIssueType(properties, "/rest/api/2.0.alpha1/issueType/1");
		System.out.println(it);
		System.out.println("Description :" + it.getDescription());
		System.out.println("Name :" + it.getName());
		System.out.println("ID :" + it.getId());
		System.out.println("Icon URI :" + it.getIconUri());
		System.out.println("SELF :" + it.getSelf());
		System.out.println("Is SubTask ? :" + it.isSubtask());
		
		// GET USER INFO
		System.out.println("\ngetUser : ");
		User user =  service.getUser(properties, "gyakornok");
		System.out.println(user);
		System.out.println("Display Name :" + user.getName());
		System.out.println("Display Name :" + user.getDisplayName());
		System.out.println("Email Address :" + user.getEmailAddress());
		System.out.println("Time Zone :" + user.getTimezone());
		System.out.println("Avatar URI :" + user.getAvatarUri());
		System.out.println("Small Avatar URI :" + user.getSmallAvatarUri());
		System.out.println("Groups :" + user.getGroups());
		System.out.println("SELF :" + user.getSelf());
		System.out.println("Is Self URI InComplete? :" + user.isSelfUriIncomplete());
		
		
		// GET ISSUE PRIORITY
		System.out.println("\ngetIssuePriority :");
		Priority prio =  service.getIssuePriority(properties, "3");
		System.out.println(prio);
		System.out.println("Name :" + prio.getName());
		System.out.println("Description :" + prio.getDescription());
		System.out.println("Status Color :" + prio.getStatusColor());
		System.out.println("ID :" + prio.getId());
		System.out.println("Icon URI :" + prio.getIconUri());
		System.out.println("SELF :" + prio.getSelf());

		// ISSUE TRANSITIONS
		System.out.println("\ngetTransitionsOfAnIssue :");
		Iterable <Transition> T2 = service.getTransitionsOfAnIssue(properties , "GYAKPROJ-2");
		for (Transition transition : T2) {
			System.out.println(transition);
			System.out.println("Transition ID:"+transition.getId());
			System.out.println("Transition Name:"+transition.getName());
			System.out.println("Transition Fields:"+transition.getFields());
		}	
		
		//service.createIssue();					//JIRA 5.0-s verziótól
		//service.doComment("Ez egy komment.");		//JIRA 5.0-s verziótól
				
		
	}

}
