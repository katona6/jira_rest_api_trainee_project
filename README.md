# JIRA REST API TRAINEE PROJECT #

## trainee, Summer Experimental Course (2013) - Become familiar with JavaEE and Java-based middleware developing: ##

### - programming JIRA server (project tracking system) in Java language, ###
### - Develop a middleware between Client & Server, ###
### - Create an own API for handle server-functions, ###
### - Use JIRA REST API for create service&status-query functions for a client in the future. ###

### - Valued with a 'Good' mark. ###
