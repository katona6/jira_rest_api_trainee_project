package JIRALekerdez;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.atlassian.jira.rest.client.GetCreateIssueMetadataOptionsBuilder;
import com.atlassian.jira.rest.client.IssueRestClient;
import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.NullProgressMonitor;
import com.atlassian.jira.rest.client.ProgressMonitor;
import com.atlassian.jira.rest.client.domain.BasicIssue;
import com.atlassian.jira.rest.client.domain.BasicProject;
import com.atlassian.jira.rest.client.domain.CimProject;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.domain.IssueType;
import com.atlassian.jira.rest.client.domain.IssuelinksType;
import com.atlassian.jira.rest.client.domain.Priority;
import com.atlassian.jira.rest.client.domain.Project;
import com.atlassian.jira.rest.client.domain.ProjectRole;
import com.atlassian.jira.rest.client.domain.Resolution;
import com.atlassian.jira.rest.client.domain.ServerInfo;
import com.atlassian.jira.rest.client.domain.Session;
import com.atlassian.jira.rest.client.domain.Status;
import com.atlassian.jira.rest.client.domain.Transition;
import com.atlassian.jira.rest.client.domain.User;
import com.atlassian.jira.rest.client.domain.Votes;
import com.atlassian.jira.rest.client.domain.Watchers;
import com.atlassian.jira.rest.client.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.internal.jersey.JerseyJiraRestClientFactory;


public class JiraServiceImpl implements IJiraServiceImpl{

	static JiraRestClient restClient;
	static ProgressMonitor pm;

	public JiraServiceImpl(Properties props) {
		try {
			getConnection(props);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	public void getConnection(Properties props) throws URISyntaxException {
		final JerseyJiraRestClientFactory factory = new JerseyJiraRestClientFactory();
		final URI jiraServerUri = new URI(props.getProperty("serverurl"));
		restClient = factory.createWithBasicHttpAuthentication(jiraServerUri, "gyakornok", "gyakornokpw");
		pm = new NullProgressMonitor();
	}

	public void getJson(String url) {
		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet post = new HttpGet(url);
		HttpResponse response = null;
		try {
			response = httpClient.execute(post);

		} catch (IOException ex) {
			ex.printStackTrace();
		}

		HttpEntity resEntity = response.getEntity();
		String json = "";
		if (resEntity != null) {
			try {
				json = EntityUtils.toString(resEntity);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		httpClient.getConnectionManager().shutdown();
		System.out.println(json.toString());
	}

	/*OBJECT-t visszaad� met�dusok  
	  ----->  */

	public Project getProjectByKey(String key){
		return restClient.getProjectClient().getProject(key, pm);
	}
	
	public Issue getIssueByKey(String key){
		return restClient.getIssueClient().getIssue(key, pm);
	}
	
	public Iterable <BasicProject> getAllProjects(){
		return restClient.getProjectClient().getAllProjects(pm);
	}
	
	public Watchers getWatchersOfAnIssue(Properties props,String IssueKey) throws URISyntaxException{
		URI jiraServerUri = new URI(props.getProperty("issueurl")+"/" +IssueKey+"/watchers");
		return restClient.getIssueClient().getWatchers(jiraServerUri, pm);
	}
	
	public Iterable <Transition> getTransitionsOfAnIssue(Properties prop, String IssueKey) throws URISyntaxException{
		URI ueri = new URI(prop.getProperty("issueurl") + "/" + IssueKey + "/transitions");
		return restClient.getIssueClient().getTransitions(ueri, pm);	
	}
	
	public Votes getVotesOfAnIssue(Properties prop,String IssueKey) throws URISyntaxException{
		URI jiraServerUri = new URI(prop.getProperty("issueurl")+"/"+IssueKey+"/votes");
		return restClient.getIssueClient().getVotes(jiraServerUri, pm);
	}
	
	public Iterable <IssuelinksType> getIssueLinkTypes(){
		return restClient.getMetadataClient().getIssueLinkTypes(pm);
	}
	
	public Iterable <Priority> getPriorities(){
		return restClient.getMetadataClient().getPriorities(pm);
	}
	
	public ServerInfo getServerInfo(){
		return restClient.getMetadataClient().getServerInfo(pm);
	}
	
	public Iterable <ProjectRole> getProjectRoles(String ProjectURI) throws URISyntaxException{
		URI jiraServerUri = new URI(ProjectURI);
		return restClient.getProjectRolesRestClient().getRoles(jiraServerUri, pm);	
	}
	
	public Session getCurrentSession(){
		return restClient.getSessionClient().getCurrentSession(pm);
	}
	
	public Status getStatusOfAnIssue(Properties prop, String RequestStatusURI) throws URISyntaxException{
		URI ueri = new URI(prop.getProperty("serverurl") + RequestStatusURI);
		return restClient.getMetadataClient().getStatus(ueri, pm);	
	}
	
	public Resolution getResolution(Properties prop, String ResolutionURI) throws URISyntaxException{
		URI ueri = new URI(prop.getProperty("serverurl") +      ResolutionURI);
		return restClient.getMetadataClient().getResolution(ueri, pm);
	}
	
	public IssueType getIssueType(Properties prop, String IssueTypeURI) throws URISyntaxException{
		URI ueri = new URI(prop.getProperty("serverurl") + IssueTypeURI);
		return restClient.getMetadataClient().getIssueType(ueri, pm);
	}
	
	public User getUser(Properties prop, String Username) throws URISyntaxException{
		URI ueri = new URI(prop.getProperty("serverurl")+ "/rest/api/2.0.alpha1/user?username="+ Username);
		return restClient.getUserClient().getUser(ueri, pm);
	}
	
	public Priority getIssuePriority(Properties prop, String PriorityID) throws URISyntaxException{
		URI ueri = new URI (prop.getProperty("serverurl") + "/rest/api/2.0.alpha1/priority/" + PriorityID);
		return restClient.getMetadataClient().getPriority(ueri, pm);
	}
	
//	public void doComment(String kommentSzoveg){
//		
//		Type type = new Type();
//		Visibility visibility = new Visibility(type, "true");
//		DateTime creationDate = new DateTime("12.08.2013");
//		DateTime updateDate = new DateTime("12.08.2013");
//		URI buURI = new URI("http://jrserv.simplesoft.hu:81/rest/api/2.0.alpha1/user?username=gyakornok");
//		BasicUser bu = new BasicUser(buURI, "gyakornok", "Gyakornok Teszt");
//		URI selfueri = new URI("http://jrserv.simplesoft.hu:81/rest/api/2.0.alpha1/comment/12613");
//		Comment comment = new Comment(selfueri, kommentSzoveg, bu, bu, creationDate, updateDate, visibility, 1l);
//		URI ueri = new URI ("http://jrserv.simplesoft.hu:81/rest/api/2.0.alpha1/comment/12613?false");
//		restClient.getIssueClient().addComment(pm, ueri, comment);
//	}
	
	public void createIssue(){
		
//		IssueRestClient issueClient = restClient.getIssueClient();
//		Iterable<CimProject> createIssueMetadata = restClient.getIssueClient()
//                .getCreateIssueMetadata(new GetCreateIssueMetadataOptionsBuilder().build(), pm);
//		System.out.println(createIssueMetadata);
//		
//		final IssueInputBuilder issueBuilder = new IssueInputBuilder("GYAKPROJ", 1l);
//		issueBuilder.setDescription("description");
//		issueBuilder.setSummary("summary");
//		
//		BasicIssue jiraIssue = issueClient.createIssue(issueBuilder.build(), pm);
//		System.out.println(jiraIssue);	
		
		
		final IssueRestClient issueClient = restClient.getIssueClient();
		final ProgressMonitor pm = new NullProgressMonitor();
		final Iterable<CimProject> createIssueMetadata = restClient.getIssueClient()
                .getCreateIssueMetadata(new GetCreateIssueMetadataOptionsBuilder().build(), pm);
		System.out.println(createIssueMetadata);
		final IssueInputBuilder issueBuilder = new IssueInputBuilder("GYAKPROJ", 1l);
		issueBuilder.setDescription("description");
        issueBuilder.setSummary("summary");
        BasicIssue jiraIssue = issueClient.createIssue(issueBuilder.build(), pm);
        System.out.println(jiraIssue);
				
	}
	
}
